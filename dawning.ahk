﻿#SingleInstance Force
F5::Reload()

#HotIf WinActive("Destiny 2", )
F9::bake_start()
bake_start()
{
    static Toggle :=
    SetTimer(bake,(Toggle:=!Toggle) ? 1250 : "Off")

    bake()
    {
        Send("{lbutton down}")
        Sleep(1000)
        Send("{lbutton up}")
        return
    }
}

; You have to watch for when postmaster is out of space
#HotIf WinActive("Destiny 2", )
F10::give_start()
give_start()
{
    static Toggle :=
    SetTimer(give,(Toggle:=!Toggle) ? 250 : "Off")

    give()
    {
        MouseClick("left")
        return
    }
}

#HotIf WinActive("Destiny 2", )
F11::vendor_start()
vendor_start()
{
    static Toggle :=
    SetTimer(vendor,(Toggle:=!Toggle) ? 1800 : "Off")

    vendor()
    {
        Send("{f down}")
        Sleep(1000)
        Send("{f up}")
        return
    }
}
