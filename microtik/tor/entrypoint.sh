#!/bin/sh

# allow entering
if [ "$1" == 'sh' ]; then
    exec /bin/ash
fi

tor --runasdaemon 0 --torrc-file /tor-data/etc/torrc