#!/bin/sh

# allow entering
if [ "$1" == 'sh' ]; then
    exec /bin/bash
fi

dropbear -Ems -p 0.0.0.0:22

cd /eggdrop
./eggdrop /eggdrop-data/eggdrop.conf
tail -f /dev/shm/console.log
