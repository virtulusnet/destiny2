#!/bin/sh

# allow entering
if [ "$1" == 'sh' ]; then
    exec /bin/ash
fi

znc --allow-root --foreground --datadir /znc-data