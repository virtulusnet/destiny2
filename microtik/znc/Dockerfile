# syntax=docker/dockerfile:1
############ build stage ############
FROM arm32v7/alpine:3.16 as buildstage

WORKDIR /src

RUN <<EOF
    apk add --no-cache \
        boost-dev \
        build-base \
        cmake \
        cyrus-sasl-dev \
        openssl3-dev \
        git
EOF

RUN <<EOF
set -xe
    git clone --depth 1 https://github.com/znc/znc.git .
    git clone --depth 1 https://github.com/CyberShadow/znc-clientbuffer.git
    git submodule update --depth 1 --init --recursive
EOF

COPY --link patches /

ARG CFLAGS="-O3 -pipe -fno-plt -funsafe-math-optimizations -ffast-math -fstack-protector -fstack-clash-protection -march=armv7ve+simd -mtune=cortex-a15 -mfloat-abi=hard -mfp16-format=ieee -mfpu=neon-vfpv4 -mabi=aapcs-linux -mtls-dialect=gnu -mthumb -mlibarch=armv7ve+simd -Wp,-D_FORTIFY_SOURCE=2"
ARG CXXFLAGS="$CFLAGS"
ARG LDFLAGS="-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now"
ARG CONFIG

RUN <<EOF
set -xe
    patch -Np1 -i /znc-allow_root.patch
    ln -s /src/znc-clientbuffer/clientbuffer.cpp /src/modules
    mkdir build && cd build
    cmake .. $CONFIG
EOF

ARG MAKEFLAGS="-j4"

RUN <<EOF
set -xe
    cd build
    make $MAKEFLAGS
    make DESTDIR=/opt install
EOF

############ runtime stage ############
FROM arm32v7/alpine:3.16

RUN <<EOF
    apk add -U --update --no-cache \
        ca-certificates \
        tini \
        libgcc \
        libstdc++ \
        zlib \
        openssl3
EOF

COPY --link --from=buildstage /opt/ /
COPY --link entrypoint.sh /

VOLUME /znc-data

ENTRYPOINT ["tini", "--", "/entrypoint.sh"]
