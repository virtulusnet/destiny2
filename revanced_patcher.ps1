# https://github.com/TeamVanced/VancedMicroG/releases

$repos = @{
    'revanced-patches'      = 'revanced-patches-REPL.jar'
    'revanced-cli'          = 'revanced-cli-REPL-all.jar'
    'revanced-integrations' = 'app-release-unsigned.apk'
}

# comment out what you dont use
$apps = @{
    'youtube' = 'youtube_17.45.36'
    'twitch'  = 'tv.twitch.android.app'
}


# dont show download bar
$ProgressPreference = 'SilentlyContinue'
# create output folder
[void](mkdir -Force @('output','lib'))


function get_tag {
    Begin {
        $url = "https://github.com/revanced/$($args[0])/releases/latest"
    }
    Process {
        # get latest patch version information
        $request = [System.Net.WebRequest]::Create($url)
        $tagUrl = $request.GetResponse().ResponseUri.OriginalString
        $version = $tagUrl.split('/')[-1].Trim('v')
    }
    End {
        return $tagUrl, $version
    }
}

# check for java in PATH
if(-Not (Get-Command 'java' -ErrorAction SilentlyContinue)) {
    Write-Output "Java not found!"
    Write-Output "https://adoptium.net/temurin/releases"
    exit
}

# get latest patch versions
$tmp = @{}
foreach ($i in $repos.GetEnumerator()) {
    $app = $i.name
    $url, $ver = get_tag $app
    $file = $($i.value).replace("REPL", $ver)
    # update new dict value with version info to construct final java command
    $tmp[$app] = $file
    $tmp[$app] = $($file).replace("unsigned", $ver)
    $url = $url.replace("tag", "download")+'/'+$file

    # only download if it doesnt exist
    if (-Not (Test-Path lib/$($tmp[$app]) -PathType Leaf)) {
        Write-Output "Downloading $app $ver"
        Invoke-WebRequest -Uri $url -OutFile lib/$($tmp[$app])
    }
}

# automatically rename the apks from apkmirror's insanity
Get-ChildItem -Filter *.apk | Where-Object { $_.Name -match "com.google.android.$($apps['youtube'])" } |
ForEach-Object {
    Move-Item $_.Name -Destination "$($apps['youtube']).apk" -Force
    Write-Output "Renamed youtube apk"
}
Get-ChildItem -Filter *.apk | Where-Object { $_.Name -match "$($apps['twitch'])" } |
ForEach-Object {
    Move-Item $_.Name -Destination "$($apps['twitch']).apk" -Force
    Write-Output "Renamed twitch apk"
}

# iterate over each app
foreach ($apk in $apps.GetEnumerator()) {
    if (Test-Path -Path "$($apk.value).apk" -PathType Leaf) {
        Write-Output "Found $($apk.value)"
    }
    else {
        switch($apk.name) {
           'youtube' {"Download $($apk.value) from https://www.apkmirror.com/apk/google-inc/youtube"}
           'twitch'  {"Download $($apk.value) from https://www.apkmirror.com/apk/twitch-interactive-inc/twitch"}
        }
        break
    }

    Write-Output "Patching apk"
    java -jar lib/$($tmp['revanced-cli']) --experimental -c `
           -m lib/$($tmp['revanced-integrations']) `
           -b lib/$($tmp['revanced-patches']) `
           --keystore=revanced.keystore `
           -a "$($apk.value).apk" `
           -o "output/$($apk.value).apk"
}
