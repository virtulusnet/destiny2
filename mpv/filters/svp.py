import math
import vapoursynth as vs
core = vs.core

core.max_cache_size = 4800
clip = video_in
src_fps    = container_fps if container_fps>0.1 else 23.976
#extension = os.path.splitext(str(clip))[1][1:]      #Catches the file extension name i.e wmv.

if round(src_fps*1000)/1000 == 23.810: #Fix for incorrect container_fps provided by MPV on Crunchyroll encodes causing audio desync
    src_fps = 23.976

###########-=SVP Config=-###########
block_size       = int(math.pow(2, round(math.log2(clip.width//80))))
block_size       = 32 if block_size > 32 else (8 if block_size < 8 else block_size)
pixel_precision  = 2 if clip.width < 2000 else 1            # Lowers the resources needed for 4K Materials.
block_overlap    = 2 if clip.width < 2000 else 0            # Lowers the resources needed for 4K Materials.
fine_search_type = 4 if clip.width < 2000 else 2            # Lowers the resources needed for 4K Materials.
algo_var = 13 if clip.width < 2000 else 1                   # Lowers the resources needed for 4K Materials.
course_search_type = 4 if clip.width < 2000 else 2          # Lowers the resources needed for 4K Materials.
distance        = -12 if clip.width < 2000 else 0           # Lowers the resources needed for 4K Materials.
refine_params = "[{thsad:200,search:{distance:" + str(distance//2) + "}},{thsad:200,search:{distance:" + str(distance//4) + "}}]" if clip.width > 1280 else "[{thsad:200,search:{distance:" + str(distance//2) + "}}]"

if clip.width >=2000 or display_fps > 60: #Lowers the resources needed for 4K Materials or high refreshrate screens.
    super_params = "{pel:1,scale:{up:0},gpu:1,full:false}"
    analyse_params = "{block:{overlap:0},main:{search:{coarse:{distance:" + str(distance) + ",bad:{sad:2000}},distance:" + str(distance) + "}}},refine:" + refine_params + "}"
    clip=core.resize.Bicubic(clip, format=vs.YUV420P8,matrix_s="709",matrix_in_s="709", filter_param_a=0, filter_param_b=0.75, range_in_s="full",range_s="full", chromaloc_in_s="center", chromaloc_s="center",dither_type="none")
else:
    super_params     = "{pel:1,scale:{up:2},gpu:1,full:false,rc:true}"
    analyse_params   = "{block:{w:" + str(block_size) + ",overlap:1},main:{search:{coarse:{distance:-12,bad:{sad:2000}},type:2}},refine:[{thsad:250}]}"
    clip=core.std.SetFrameProp(clip, prop="_ColorRange", intval=0)
    clip=core.resize.Spline64(clip, format=vs.YUV444P16, matrix_in_s="709", range_s="full",dither_type="error_diffusion")
    clip=core.resize.Spline64(clip,format=vs.YUV420P8,matrix_in_s="709", filter_param_a=0.33, filter_param_b=0.33, range_s="full", chromaloc_s="center",dither_type="error_diffusion")

def interpolate(clip):

    display_hz = display_fps
    if display_hz > 120:
        display_hz = 120

    display_hz_num = int(display_hz * 1e4)
    display_hz_den = int(1e4)
    smoothfps_params = "{debug:{vectors:false},rate:{abs:true,den:" + str(display_hz_den) + ",num:" + str(display_hz_num) + ",algo:" + str(algo_var) + "},cubic:1,gpuid:0,block:false,linear:true,mask:{area:100,cover:150,area_sharp:1.0},scene:{mode:3,blend:true,limits:{zero:200,blocks:50}}}"
    super   = core.svp1.Super(clip,super_params)
    vectors = core.svp1.Analyse(super["clip"],super["data"],clip,analyse_params)
    smooth  = core.svp2.SmoothFps(clip,super["clip"],super["data"],vectors["clip"],vectors["data"],smoothfps_params,src=clip,fps=src_fps)
    smooth  = core.std.AssumeFPS(smooth,fpsnum=smooth.fps_num,fpsden=smooth.fps_den)
    return smooth

smooth =  interpolate(clip)
smooth.set_output()
